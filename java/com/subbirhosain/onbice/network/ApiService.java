package com.subbirhosain.onbice.network;

import com.subbirhosain.onbice.model.AddNotice;
import com.subbirhosain.onbice.model.Categories;
import com.subbirhosain.onbice.model.DeleteResponse;
import com.subbirhosain.onbice.model.FcmTopicResponse;
import com.subbirhosain.onbice.model.FileUpload;
import com.subbirhosain.onbice.model.LoginResponse;
import com.subbirhosain.onbice.model.NoticeResponse;
import com.subbirhosain.onbice.model.Notices;
import com.subbirhosain.onbice.model.RegisterDevices;
import com.subbirhosain.onbice.model.RegisterUser;
import com.subbirhosain.onbice.model.SearchNoticeResponse;
import com.subbirhosain.onbice.model.SendTopicResponse;
import com.subbirhosain.onbice.model.SingleNoticeResponse;
import com.subbirhosain.onbice.model.UpdateResponse;


import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Query;

/**
 * Created by SubbirHosain on 1/22/2018.
 */

public interface ApiService {

    //login here
    @FormUrlEncoded
    @POST("login.php")
    Call<LoginResponse> login(
            @Field("email") String Email,
            @Field("password") String Password
    );

    //Register fcm token
    @FormUrlEncoded
    @POST("insertTokenToServer.php")
    Call<RegisterDevices> sendRegToServer(
            @Field("email") String Email,
            @Field("token") String Token
    );

    //Getting Notices
    @GET("getNotices.php")
    Call<List<NoticeResponse>> getNoticeLists(@Query("page_number") int page, @Query("item_count") int items);

    //getting spinner batches
    @GET("getBatch.php")
    Call<Categories> getBatches();

    //Register user
    @FormUrlEncoded
    @POST("registerUser.php")
    Call<RegisterUser> registerUser(
            @Field("user_email") String Email,
            @Field("fname") String Fname,
            @Field("lname") String Lname,
            @Field("user_password") String Passowrd,
            @Field("user_session") String Session
    );

    //Uploading featured image
    @Multipart
    @POST("fileUpload.php")
    Call<FileUpload> uploadImage(@Part MultipartBody.Part file, @Part("file") RequestBody name);

    //Adding Notice
    @FormUrlEncoded
    @POST("add_notice.php")
    Call<AddNotice> addNotice(
            @Field("notice_title") String NoticeTitle,
            @Field("notice_content") String NoticeContent,
            @Field("notice_category") String NoticeCategory,
            @Field("notice_date") String NoticeDate,
            @Field("notice_featured_image") String FeaturedImage
    );

    //get fcm topics
    @GET("getFcmTopics.php")
    Call<FcmTopicResponse> getFcmTopics();


    //send fcm topics batch notification
    @FormUrlEncoded
    @POST("sendBatchNotification.php")
    Call<ResponseBody> sendTopicNotification(
            @Field("title") String Title,
            @Field("message") String Message,
            @Field(("topic")) String Topic,
            @Field("action_destination") String Link
    );

    //delete notice
    @GET("deleteNotice.php")
    Call<DeleteResponse> deleteNotice(@Query("notice_id") String NoticeId);

    //get single notice using id
    @GET("getSingleNotice.php")
    Call<SingleNoticeResponse> getSingleNotice(@Query("notice_id") String NoticeId);


    //updating the notice
    @GET("updateNotice.php")
    Call<UpdateResponse> updateNoticeOld(@Query("notice_id") String NoticeID, @Query("notice_title") String NotticeTitle, @Query("notice_content") String NoticeContent, @Query("notice_category") String NoticeCategory, @Query("notice_date") String NoticeDate, @Query("notice_featured_image") String FeaturedImage);

    //get single notice using id
    @GET("noticeSearch.php")
    Call<SearchNoticeResponse> getOldNotices(@Query("from_date") String fromDate, @Query("to_date") String toDate);

}

