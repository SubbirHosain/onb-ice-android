package com.subbirhosain.onbice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NoticeResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("notices")
    @Expose
    private List<Notices> notices = null;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Notices> getNotices() {
        return notices;
    }

    public void setNotices(List<Notices> notices) {
        this.notices = notices;
    }

}

