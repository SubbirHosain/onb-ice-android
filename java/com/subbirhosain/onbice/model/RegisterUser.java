package com.subbirhosain.onbice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterUser {

    @SerializedName("register_error")
    @Expose
    private Boolean registerError;
    @SerializedName("register_message")
    @Expose
    private String registerMessage;

    public Boolean getRegisterError() {
        return registerError;
    }

    public void setRegisterError(Boolean registerError) {
        this.registerError = registerError;
    }

    public String getRegisterMessage() {
        return registerMessage;
    }

    public void setRegisterMessage(String registerMessage) {
        this.registerMessage = registerMessage;
    }

}