
package com.subbirhosain.onbice.model;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SearchNoticeResponse {

    @SerializedName("status")
    @Expose
    private Boolean status;
    @SerializedName("notices")
    @Expose
    private List<Notice> notices = null;

    public Boolean getStatus() {
        return status;
    }

    public void setStatus(Boolean status) {
        this.status = status;
    }

    public List<Notice> getNotices() {
        return notices;
    }

    public void setNotices(List<Notice> notices) {
        this.notices = notices;
    }

}
