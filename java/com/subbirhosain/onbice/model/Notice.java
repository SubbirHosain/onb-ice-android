
package com.subbirhosain.onbice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Notice {

    @SerializedName("notice_id")
    @Expose
    private String noticeId;
    @SerializedName("notice_title")
    @Expose
    private String noticeTitle;
    @SerializedName("notice_content")
    @Expose
    private String noticeContent;
    @SerializedName("notice_category_id")
    @Expose
    private String noticeCategoryId;
    @SerializedName("notice_date")
    @Expose
    private String noticeDate;
    @SerializedName("notice_status")
    @Expose
    private String noticeStatus;
    @SerializedName("notice_featured_image")
    @Expose
    private String noticeFeaturedImage;

    public String getNoticeId() {
        return noticeId;
    }

    public void setNoticeId(String noticeId) {
        this.noticeId = noticeId;
    }

    public String getNoticeTitle() {
        return noticeTitle;
    }

    public void setNoticeTitle(String noticeTitle) {
        this.noticeTitle = noticeTitle;
    }

    public String getNoticeContent() {
        return noticeContent;
    }

    public void setNoticeContent(String noticeContent) {
        this.noticeContent = noticeContent;
    }

    public String getNoticeCategoryId() {
        return noticeCategoryId;
    }

    public void setNoticeCategoryId(String noticeCategoryId) {
        this.noticeCategoryId = noticeCategoryId;
    }

    public String getNoticeDate() {
        return noticeDate;
    }

    public void setNoticeDate(String noticeDate) {
        this.noticeDate = noticeDate;
    }

    public String getNoticeStatus() {
        return noticeStatus;
    }

    public void setNoticeStatus(String noticeStatus) {
        this.noticeStatus = noticeStatus;
    }

    public String getNoticeFeaturedImage() {
        return noticeFeaturedImage;
    }

    public void setNoticeFeaturedImage(String noticeFeaturedImage) {
        this.noticeFeaturedImage = noticeFeaturedImage;
    }

}
