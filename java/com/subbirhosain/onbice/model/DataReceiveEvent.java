package com.subbirhosain.onbice.model;

import java.util.List;

public class DataReceiveEvent {

    private String eventTag;
    List<Notice> noticeList;
    String msg;

    public DataReceiveEvent(String eventTag, List<Notice> noticeList, String msg) {
        this.eventTag = eventTag;
        this.noticeList = noticeList;
        this.msg = msg;
    }

    public String getEventTag() {
        return eventTag;
    }

    public List<Notice> getNoticeList() {
        return noticeList;
    }

    public String getMsg() {
        return msg;
    }
}