package com.subbirhosain.onbice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class FcmTopicResponse {

    @SerializedName("topics")
    @Expose
    private List<Topic> topics = null;

    public List<Topic> getTopics() {
        return topics;
    }

    public void setTopics(List<Topic> topics) {
        this.topics = topics;
    }
}