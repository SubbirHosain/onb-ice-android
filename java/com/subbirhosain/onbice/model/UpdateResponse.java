package com.subbirhosain.onbice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpdateResponse {
    @SerializedName("notice_error")
    @Expose
    private Boolean noticeError;
    @SerializedName("notice_message")
    @Expose
    private String noticeMessage;

    public Boolean getNoticeError() {
        return noticeError;
    }

    public void setNoticeError(Boolean noticeError) {
        this.noticeError = noticeError;
    }

    public String getNoticeMessage() {
        return noticeMessage;
    }

    public void setNoticeMessage(String noticeMessage) {
        this.noticeMessage = noticeMessage;
    }

}
