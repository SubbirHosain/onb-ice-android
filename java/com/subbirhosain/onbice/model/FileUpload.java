
package com.subbirhosain.onbice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class FileUpload {

    @SerializedName("image_url")
    @Expose
    private String imageUrl;
    @SerializedName("success")
    @Expose
    private Boolean success;
    @SerializedName("upload_message")
    @Expose
    private String uploadMessage;

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public Boolean getSuccess() {
        return success;
    }

    public void setSuccess(Boolean success) {
        this.success = success;
    }

    public String getUploadMessage() {
        return uploadMessage;
    }

    public void setUploadMessage(String uploadMessage) {
        this.uploadMessage = uploadMessage;
    }

}
