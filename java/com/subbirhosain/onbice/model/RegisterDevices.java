package com.subbirhosain.onbice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class RegisterDevices {
    @SerializedName("fcm_error")
    @Expose
    private Boolean fcmError;
    @SerializedName("fcm_message")
    @Expose
    private String fcmMessage;

    public Boolean getFcmError() {
        return fcmError;
    }

    public void setFcmError(Boolean fcmError) {
        this.fcmError = fcmError;
    }

    public String getFcmMessage() {
        return fcmMessage;
    }

    public void setFcmMessage(String fcmMessage) {
        this.fcmMessage = fcmMessage;
    }
}
