package com.subbirhosain.onbice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SingleNoticeResponse {

    @SerializedName("notice_error")
    @Expose
    private Boolean noticeError;
    @SerializedName("notice_content")
    @Expose
    private NoticeContent noticeContent;

    public Boolean getNoticeError() {
        return noticeError;
    }

    public void setNoticeError(Boolean noticeError) {
        this.noticeError = noticeError;
    }

    public NoticeContent getNoticeContent() {
        return noticeContent;
    }

    public void setNoticeContent(NoticeContent noticeContent) {
        this.noticeContent = noticeContent;
    }

}