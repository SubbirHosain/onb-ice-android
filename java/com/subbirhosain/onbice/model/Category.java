
package com.subbirhosain.onbice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Category {

    @SerializedName("notice_cat_id")
    @Expose
    private String noticeCatId;
    @SerializedName("notice_cat_name")
    @Expose
    private String noticeCatName;
    @SerializedName("notice_cat_session")
    @Expose
    private String noticeCatSession;
    @SerializedName("notice_cat_description")
    @Expose
    private String noticeCatDescription;

    public String getNoticeCatId() {
        return noticeCatId;
    }

    public void setNoticeCatId(String noticeCatId) {
        this.noticeCatId = noticeCatId;
    }

    public String getNoticeCatName() {
        return noticeCatName;
    }

    public void setNoticeCatName(String noticeCatName) {
        this.noticeCatName = noticeCatName;
    }

    public String getNoticeCatSession() {
        return noticeCatSession;
    }

    public void setNoticeCatSession(String noticeCatSession) {
        this.noticeCatSession = noticeCatSession;
    }

    public String getNoticeCatDescription() {
        return noticeCatDescription;
    }

    public void setNoticeCatDescription(String noticeCatDescription) {
        this.noticeCatDescription = noticeCatDescription;
    }

}
