package com.subbirhosain.onbice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by SubbirHosain on 1/30/2018.
 */

public class LoginResponse {
    @SerializedName("login_error")
    @Expose
    private Boolean loginError;
    @SerializedName("user_email")
    @Expose
    private String userEmail;
    @SerializedName("user_session")
    @Expose
    private String userSession;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("user_type")
    @Expose
    private String userType;
    @SerializedName("login_message")
    @Expose
    private String loginMessage;

    public Boolean getLoginError() {
        return loginError;
    }

    public void setLoginError(Boolean loginError) {
        this.loginError = loginError;
    }

    public String getUserEmail() {
        return userEmail;
    }

    public void setUserEmail(String userEmail) {
        this.userEmail = userEmail;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLoginMessage() {
        return loginMessage;
    }

    public void setLoginMessage(String loginMessage) {
        this.loginMessage = loginMessage;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getUserSession() {
        return userSession;
    }

    public void setUserSession(String userSession) {
        this.userSession = userSession;
    }
}