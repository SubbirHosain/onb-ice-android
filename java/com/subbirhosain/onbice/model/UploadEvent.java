package com.subbirhosain.onbice.model;

public class UploadEvent{

    private boolean isUploaded;

    private String  responseMessage;

    public UploadEvent(boolean success, String message){
        this.isUploaded = success;
        this.responseMessage = message;
    }

    public boolean isUploaded(){
        return isUploaded;
    }

    public String getResponseMessage(){
        return responseMessage;
    }
}