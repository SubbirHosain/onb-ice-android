package com.subbirhosain.onbice.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DeleteResponse {

    @SerializedName("delete_error")
    @Expose
    private Boolean deleteError;
    @SerializedName("delete_message")
    @Expose
    private String deleteMessage;

    public Boolean getDeleteError() {
        return deleteError;
    }

    public void setDeleteError(Boolean deleteError) {
        this.deleteError = deleteError;
    }

    public String getDeleteMessage() {
        return deleteMessage;
    }

    public void setDeleteMessage(String deleteMessage) {
        this.deleteMessage = deleteMessage;
    }

}