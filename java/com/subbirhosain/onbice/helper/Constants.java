package com.subbirhosain.onbice.helper;

/**
 * Created by SubbirHosain on 2/3/2018.
 */

public class Constants {

    public static final String CHANNEL_ID = "onbchannelid";
    public static final String CHANNEL_NAME = "onbnotification";
    public static final String CHANNEL_DESCRIPTION = "ONB Notification";


}
