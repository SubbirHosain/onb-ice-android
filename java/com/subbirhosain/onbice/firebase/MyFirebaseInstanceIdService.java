package com.subbirhosain.onbice.firebase;

import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.google.firebase.messaging.FirebaseMessaging;
import com.subbirhosain.onbice.helper.SharedPrefManager;


/**
 * Created by SubbirHosain on 2/3/2018.
 */

public class MyFirebaseInstanceIdService extends FirebaseInstanceIdService {

    private static final String TAG = "MyFirebaseIIDService";

    @Override
    public void onTokenRefresh() {

        //Getting registration token
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();


        Boolean update = false;
        //Displaying token on logcat
        Log.d(TAG, "Refreshed token: " + refreshedToken);
        //calling the method store token and passing token
        storeToken(refreshedToken);
        // now subscribe to `global` topic to receive app wide notifications
        FirebaseMessaging.getInstance().subscribeToTopic("global");

        tokenStatus(update);
    }


    private void storeToken(String token) {
        //we will save the token in sharedpreferences
        SharedPrefManager.getInstance(getApplicationContext()).saveDeviceToken(token);
    }
    private void tokenStatus(Boolean status) {
        SharedPrefManager.getInstance(getApplicationContext()).saveTokenStatus(status);
    }

}
