package com.subbirhosain.onbice.firebase;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.media.RingtoneManager;
import android.net.Uri;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.text.Html;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.activity.DetailActivity;
import com.subbirhosain.onbice.activity.MainActivity;
import com.subbirhosain.onbice.activity.NotificationActionActivity;
import com.subbirhosain.onbice.activity.NotificationActivity;
import com.subbirhosain.onbice.helper.Constants;
import com.subbirhosain.onbice.helper.MyNotificationManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Map;

import static com.subbirhosain.onbice.helper.Constants.CHANNEL_ID;


/**
 * Created by SubbirHosain on 2/3/2018.
 */

public class MyFirebaseMessagingService extends FirebaseMessagingService {
    private static final String TAG = "MyFirebaseMsgService";
    Bitmap bitmap;
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        // Check if message contains a data payload.
        if (remoteMessage.getData().size() > 0) {

            Log.e(TAG, "Data Payload: " + remoteMessage.getData());

            String title = remoteMessage.getData().get("title");
            String message = remoteMessage.getData().get("message");
            String imageUrl = remoteMessage.getData().get("image");
            String noticeUrl = remoteMessage.getData().get("action_destination");


            //if there is no image
           if (imageUrl.equals(null) || imageUrl.equals("")){
                //displaying BigTextStyle notification
                createNotificationChannel();
                sendBigTextNotification(title,message,noticeUrl);
            }
            else{
                createNotificationChannel();
               //To get a Bitmap image from the URL received
                bitmap = getBitmapfromUrl(imageUrl);
                sendBitPictureNotification(title,message,bitmap);
            }
            //Map<String, String> data = remoteMessage.getData();
            //sendPushNotification(data);

        }

    }

    private void createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O){
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel notificationChannel = new NotificationChannel(Constants.CHANNEL_ID,Constants.CHANNEL_NAME,importance);

            notificationChannel.setDescription(Constants.CHANNEL_DESCRIPTION);

            NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            if (notificationManager != null) {
                notificationManager.createNotificationChannel(notificationChannel);
            }
        }
    }

    private void sendBitPictureNotification(String title, String message, Bitmap bitmap) {

        Intent intent = new Intent(this, MainActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0 /* Request code */, intent,
                PendingIntent.FLAG_ONE_SHOT);

        Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
        NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this, Constants.CHANNEL_ID)
                .setLargeIcon(bitmap)/*Notification icon image*/
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setContentText(message)
                .setStyle(new NotificationCompat.BigPictureStyle()
                        .bigPicture(bitmap))/*Notification with Image*/
                .setAutoCancel(true)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent);

        NotificationManager notificationManager =
                (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);

        notificationManager.notify(0 /* ID of notification */, notificationBuilder.build());

    }

    private void sendBigTextNotification(String title, String message, String noticeUrl) {

        //To set large icon in notification
        Bitmap licon = BitmapFactory.decodeResource(getResources(), R.drawable.ic_notifications_active_black_24dp);

        //Assign BigText style notification
        NotificationCompat.BigTextStyle bigText = new NotificationCompat.BigTextStyle();
        bigText.bigText(message);
        bigText.setSummaryText("By: ONB ICE");

        // Set the intent to fire when the user taps on notification.
        Intent intent = new Intent(this, NotificationActionActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        intent.putExtra("LINK",noticeUrl);
        PendingIntent pendingIntent = PendingIntent.getActivity(this,0, intent,PendingIntent.FLAG_UPDATE_CURRENT);
        long[] pattern = {500,500,500,500,500};

        Uri defaultSoundUri = Uri.parse(ContentResolver.SCHEME_ANDROID_RESOURCE
                + "://" + getPackageName() + "/raw/notification");

        NotificationCompat.Builder mBuilder =  new NotificationCompat.Builder(this,Constants.CHANNEL_ID)
                .setSmallIcon(R.mipmap.ic_launcher)
                .setContentTitle(title)
                .setLargeIcon(licon)
                .setAutoCancel(true)
                .setOngoing(true)
                .setVibrate(pattern)
                .setSound(defaultSoundUri)
                .setContentIntent(pendingIntent)
                .setLights(Color.RED,1,1)
                .setStyle(bigText);

        // Sets an ID for the notification
        int mNotificationId = 001;
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // It will display the notification in notification bar
        notificationManager.notify(mNotificationId, mBuilder.build());

    }


    /*
     *To get a Bitmap image from the URL received
     * */
    public Bitmap getBitmapfromUrl(String imageUrl) {
        try {
            URL url = new URL(imageUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap bitmap = BitmapFactory.decodeStream(input);
            return bitmap;

        } catch (Exception e) {
            e.printStackTrace();
            return null;

        }
    }

}