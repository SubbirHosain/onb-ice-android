package com.subbirhosain.onbice.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.activity.DetailActivity;
import com.subbirhosain.onbice.model.Notices;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.List;

/**
 * Created by SubbirHosain on 1/30/2018.
 */

public class NoticeAdapter extends RecyclerView.Adapter<NoticeAdapter.NoticeViewHolder> {
    private Context context;
    private List<Notices> noticesList;

    public NoticeAdapter(Context context, List<Notices> noticesList) {
        this.context = context;
        this.noticesList = noticesList;
    }

    @Override
    public NoticeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.post_item, parent, false);
        return new NoticeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NoticeViewHolder holder, int position) {
        final Notices notices = noticesList.get(position);
        //notice title
        holder.postTitle.setText(notices.getNoticeTitle());
        //getting the notice content
        Document document = Jsoup.parse(notices.getNoticeContent());
        holder.postDescription.setText(document.text());

        //getting the image
        //Elements elements = document.select("img");
       // Glide.with(context).load(elements.get(0).attr("src")).into(holder.postImage);
        //holder.postImage.setImageURI(Uri.parse(item.getNoticePhotopath()));
        Glide.with(context).load(notices.getNoticeFeaturedImage())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.loading)
                        .centerCrop()
                        .dontAnimate()
                        .dontTransform())
                .into(holder.postImage);
        //if post clicked
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                String working_url = "http://subbirhosain.com/onb_final/view_notice.php?id="+notices.getNoticeId();
                intent.putExtra("url", working_url);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return noticesList.size();
    }

    public  class NoticeViewHolder extends RecyclerView.ViewHolder{
        ImageView postImage;
        TextView postTitle;
        TextView postDescription;
        public NoticeViewHolder(View itemView) {
            super(itemView);
            postImage = (ImageView) itemView.findViewById(R.id.postImage);
            postTitle = (TextView) itemView.findViewById(R.id.postTitle);
            postDescription = (TextView) itemView.findViewById(R.id.postDescription);
        }
    }

    //Another method for pagination
    public void addPosts(List<Notices> notices){

        for(Notices notice : notices){
           noticesList.add(notice);
        }
        notifyDataSetChanged();
}
}
