package com.subbirhosain.onbice.adapter;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.activity.DetailActivity;
import com.subbirhosain.onbice.activity.UpdateNoticeActivity;
import com.subbirhosain.onbice.model.DeleteResponse;
import com.subbirhosain.onbice.model.Notices;
import com.subbirhosain.onbice.network.ApiClient;
import com.subbirhosain.onbice.network.ApiService;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoticeModifyAdapter extends RecyclerView.Adapter<NoticeModifyAdapter.NoticeViewHolder> {
    private Context context;
    private List<Notices> noticesList;

    public NoticeModifyAdapter(Context context, List<Notices> noticesList) {
        this.context = context;
        this.noticesList = noticesList;
    }

    @Override
    public NoticeModifyAdapter.NoticeViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.notice_modify_item, parent, false);
        return new NoticeModifyAdapter.NoticeViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NoticeModifyAdapter.NoticeViewHolder holder, int position) {
        final Notices notices = noticesList.get(position);
        //notice title
        holder.noticeTitle.setText(notices.getNoticeTitle());
        //getting the notice content
        Document document = Jsoup.parse(notices.getNoticeContent());
        holder.noticeDescription.setText(document.text());

        Glide.with(context).load(notices.getNoticeFeaturedImage())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.loading)
                        .centerCrop()
                        .dontAnimate()
                        .dontTransform())
                .into(holder.noticeImage);
        //if post clicked
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                String working_url = "http://subbirhosain.com/onb_final/view_notice.php?id=" + notices.getNoticeId();
                intent.putExtra("url", working_url);
                context.startActivity(intent);
            }
        });

    }

    @Override
    public int getItemCount() {
        return noticesList.size();
    }


    public class NoticeViewHolder extends RecyclerView.ViewHolder implements View.OnCreateContextMenuListener {
        ImageView noticeImage;
        TextView noticeTitle;
        TextView noticeDescription;

        CardView modifyCardView;

        public NoticeViewHolder(View itemView) {
            super(itemView);
            noticeImage = (ImageView) itemView.findViewById(R.id.notice_modify_image);
            noticeTitle = (TextView) itemView.findViewById(R.id.notice_modify_title);
            noticeDescription = (TextView) itemView.findViewById(R.id.notice_modify_content);

            modifyCardView = itemView.findViewById(R.id.actionNoticeMenu);
            modifyCardView.setOnCreateContextMenuListener(this);
        }

        @Override
        public void onCreateContextMenu(ContextMenu contextMenu, View view, ContextMenu.ContextMenuInfo contextMenuInfo) {
            contextMenu.setHeaderTitle("Select an option");
            contextMenu.add(this.getAdapterPosition(), 120, 0, "Copy");
            contextMenu.add(this.getAdapterPosition(), 121, 1, "Update");
            contextMenu.add(this.getAdapterPosition(), 122, 2, "Delete");
        }
    }

    //Another method for pagination
    public void addNotices(List<Notices> notices) {

        for (Notices notice : notices) {
            noticesList.add(notice);
        }
        notifyDataSetChanged();
    }

    //Delete from db and recylerview also

    public void removeNotice(int position) {
        //capturing the groupId notice id
        //Toast.makeText(context,i,Toast.LENGTH_LONG).show();
        String notice_id = noticesList.get(position).getNoticeId();
        deleteNoticeFromDb(notice_id);
        noticesList.remove(position);
        notifyDataSetChanged();
    }

    //copy notice link

    public String getNoticeLink(int position) {

        String notice_id = noticesList.get(position).getNoticeId();

        return "http://subbirhosain.com/onb_final/view_notice.php?id=" + notice_id;
    }

    //update notice from db
    public void editNotice(int position) {
        String notice_id = noticesList.get(position).getNoticeId();
        Intent updateIntent = new Intent(context, UpdateNoticeActivity.class);
        updateIntent.putExtra("notice_id", notice_id);
        context.startActivity(updateIntent);
    }

    private void deleteNoticeFromDb(String notice_id) {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<DeleteResponse> call = service.deleteNotice(notice_id);

        call.enqueue(new Callback<DeleteResponse>() {
            @Override
            public void onResponse(Call<DeleteResponse> call, Response<DeleteResponse> response) {
                Toast.makeText(context, response.body().getDeleteMessage(), Toast.LENGTH_LONG).show();
            }

            @Override
            public void onFailure(Call<DeleteResponse> call, Throwable t) {

            }
        });
    }

}
