package com.subbirhosain.onbice.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.activity.DetailActivity;
import com.subbirhosain.onbice.model.Notice;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import java.util.List;

public class NoticeSearchAdapter extends RecyclerView.Adapter<NoticeSearchAdapter.NoticeSearchViewHolder> {

    private Context context;
    private List<Notice> noticeList;

    public NoticeSearchAdapter(Context context, List<Notice> noticeList) {
        this.context = context;
        this.noticeList = noticeList;
    }

    @Override
    public NoticeSearchViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        LayoutInflater inflater = LayoutInflater.from(context);
        View view = inflater.inflate(R.layout.post_item, parent, false);

        return new NoticeSearchViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NoticeSearchViewHolder holder, int position) {
        final Notice notice = noticeList.get(position);

        holder.postTitle.setText(notice.getNoticeTitle());
        Document document = Jsoup.parse(notice.getNoticeContent());
        holder.postDescription.setText(document.text());

        Glide.with(context).load(notice.getNoticeFeaturedImage())
                .apply(new RequestOptions()
                        .placeholder(R.drawable.loading)
                        .centerCrop()
                        .dontAnimate()
                        .dontTransform())
                .into(holder.postImage);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(context, DetailActivity.class);
                String working_url = "http://subbirhosain.com/onb_final/view_notice.php?id=" + notice.getNoticeId();
                intent.putExtra("url", working_url);
                context.startActivity(intent);
            }
        });
    }

    @Override
    public int getItemCount() {
        return noticeList.size();
    }

    public class NoticeSearchViewHolder extends RecyclerView.ViewHolder {

        ImageView postImage;
        TextView postTitle;
        TextView postDescription;

        public NoticeSearchViewHolder(View itemView) {
            super(itemView);
            postImage = (ImageView) itemView.findViewById(R.id.postImage);
            postTitle = (TextView) itemView.findViewById(R.id.postTitle);
            postDescription = (TextView) itemView.findViewById(R.id.postDescription);
        }
    }
}
