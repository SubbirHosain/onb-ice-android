package com.subbirhosain.onbice.activity;

import android.content.Intent;
import android.graphics.Color;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;


import com.github.ybq.android.spinkit.SpinKitView;
import com.google.firebase.messaging.FirebaseMessaging;
import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.adapter.NoticeAdapter;
import com.subbirhosain.onbice.helper.ConnectivityReceiver;
import com.subbirhosain.onbice.helper.MyApplication;
import com.subbirhosain.onbice.helper.SessionManager;
import com.subbirhosain.onbice.model.NoticeResponse;
import com.subbirhosain.onbice.model.Notices;
import com.subbirhosain.onbice.network.ApiClient;
import com.subbirhosain.onbice.network.ApiService;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity implements ConnectivityReceiver.ConnectivityReceiverListener {
    private static final String TAG = MainActivity.class.getSimpleName();
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;

    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    NoticeAdapter noticeAdapter;

    private SessionManager session;
    //for snackbar without cordinate layout
    View snackView;

    private int page_number = 1;
    private int item_count = 5;

    //variable for pagination
    private boolean isLoading = true;
    private int pastVisibleItems, visibleItemCount, totallItemCount, previousTotal = 0;
    private int view_threshold = 5;

    SpinKitView progress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //snackbar view
        snackView = findViewById(android.R.id.content);
        checkConnection();

        progress = findViewById(R.id.spin_kit);


        //for logout
        // session manager
        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {
            logoutUser();
        }

        //subscribe fcm accroding to batch session
        String individual_session = session.gettUserSession();
        if (session.gettUserSession().equals(individual_session)) {
            FirebaseMessaging.getInstance().subscribeToTopic(individual_session);
        }

        //RecylerView
        recyclerView = (RecyclerView) findViewById(R.id.postList);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        setUpToolbar();
        navigationView = (NavigationView) findViewById(R.id.navigation_menu);

        //changing navigation menu depending on user type
        String userType = session.getUserType();
        //Log.e(TAG, "UserType: " + userType);
        if (userType.equals("Student")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.navigation_user);
        }
        if (userType.equals("Admin")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.navigation_admin);
        }
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        Intent siteIntent = new Intent(MainActivity.this, DeptSiteActivity.class);
                        startActivity(siteIntent);
                        break;
                    case R.id.nav_add_notice:
                        Intent add_notice_intent = new Intent(getApplicationContext(), PublishNoticeActivity.class);
                        startActivity(add_notice_intent);
                        break;
                    case R.id.nav_allnotices:
                        Intent notice_modify_intent = new Intent(getApplicationContext(), NoticeModifyActivity.class);
                        startActivity(notice_modify_intent);
                        break;
                    case R.id.nav_logout:
                        logoutUser();
                        break;
                    case R.id.nav_notify:
                        Intent intent = new Intent(MainActivity.this, NotificationActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_settings:
                        Intent i = new Intent(MainActivity.this, SettingsActivity.class);
                        startActivity(i);
                        break;
                    case R.id.nav_send_notification:
                        Intent sendIntent = new Intent(MainActivity.this, SendNotificationActivity.class);
                        startActivity(sendIntent);
                        break;
                    case R.id.nav_aboutus:
                        Intent intentAbout = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(intentAbout);
                        break;
                }

                return false;
            }
        });

        //action when pull down swipe refresh


        //getData here for recylerview
        getData();

        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                visibleItemCount = layoutManager.getChildCount();
                totallItemCount = layoutManager.getItemCount();
                pastVisibleItems = layoutManager.findFirstVisibleItemPosition();

                if (dy > 0) {
                    if (isLoading) {
                        if (totallItemCount > previousTotal) {
                            isLoading = false;
                            previousTotal = totallItemCount;
                        }
                    }
                    if (!isLoading && (totallItemCount - visibleItemCount) <= (pastVisibleItems + view_threshold)) {
                        {
                            page_number++;
                            performPagination();
                            isLoading = true;
                        }
                    }
                }
            }
        });
    }
    //action menu setting

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent i = new Intent(MainActivity.this, SettingsActivity.class);
            startActivity(i);
        }
        if (id == R.id.about_us) {

            Intent notice = new Intent(MainActivity.this, AboutActivity.class);
            startActivity(notice);
        }
        if (id == R.id.search_notice) {

            Intent notice_sarch = new Intent(MainActivity.this, NoticeSearchActivity.class);
            startActivity(notice_sarch);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpToolbar() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    private void getData() {

        progress.setVisibility(View.VISIBLE);

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<List<NoticeResponse>> call = service.getNoticeLists(page_number, item_count);

        call.enqueue(new Callback<List<NoticeResponse>>() {
            @Override
            public void onResponse(Call<List<NoticeResponse>> call, Response<List<NoticeResponse>> response) {

                List<Notices> notices = response.body().get(1).getNotices();
                noticeAdapter = new NoticeAdapter(MainActivity.this, notices);
                recyclerView.setAdapter(noticeAdapter);
                //Toast.makeText(getApplicationContext(),"First page loaded",Toast.LENGTH_LONG).show();
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onFailure(Call<List<NoticeResponse>> call, Throwable t) {

            }
        });

    }

    private void performPagination() {

        progress.setVisibility(View.VISIBLE);

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<List<NoticeResponse>> call = service.getNoticeLists(page_number, item_count);

        call.enqueue(new Callback<List<NoticeResponse>>() {
            @Override
            public void onResponse(Call<List<NoticeResponse>> call, Response<List<NoticeResponse>> response) {

                if (response.body().get(0).getStatus().equals("ok")) {
                    List<Notices> notices = response.body().get(1).getNotices();
                    noticeAdapter.addPosts(notices);
                    //Toast.makeText(getApplicationContext(), "Page" + page_number + "is loaded", Toast.LENGTH_LONG).show();

                    progress.setVisibility(View.GONE);
                } else {
                    //Toast.makeText(getApplicationContext(), "No more notices!", Toast.LENGTH_LONG).show();

                    progress.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<List<NoticeResponse>> call, Throwable t) {

            }
        });
    }

    /**
     * Logging out the user. Will set isLoggedIn flag to false in shared
     * preferences Clears the user data from sqlite users table
     */

    private void logoutUser() {
        session.setLogin(false);

        // Launching the login activity
        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    // Method to manually check connection status
    private void checkConnection() {
        boolean isConnected = ConnectivityReceiver.isConnected();
        showSnack(isConnected);
    }

    // Showing the status in Snackbar
    private void showSnack(boolean isConnected) {
        String message;
        int color;

        if (isConnected) {
            message = "Connected to Internet";
            color = Color.WHITE;
        } else {
            message = "Sorry! Not connected to internet";
            color = Color.RED;
            Snackbar snackbar = Snackbar
                    .make(snackView, message, Snackbar.LENGTH_LONG)
                    .setAction("RETRY", new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            Intent intent = new Intent(Settings.ACTION_WIRELESS_SETTINGS);
                            startActivity(intent);
                        }
                    });
            View sbView = snackbar.getView();
            TextView textView = (TextView) sbView.findViewById(android.support.design.R.id.snackbar_text);
            textView.setTextColor(color);
            snackbar.show();
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        // register connection status listener
        MyApplication.getInstance().setConnectivityListener(this);
    }

    /**
     * Callback will be triggered when there is change in
     * network connection
     */
    @Override
    public void onNetworkConnectionChanged(boolean isConnected) {

        showSnack(isConnected);
    }

    //intent transition
    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }

}
