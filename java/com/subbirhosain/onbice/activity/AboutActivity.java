package com.subbirhosain.onbice.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.helper.SessionManager;

public class AboutActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    private SessionManager session;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {
            logoutUser();
        }

        setUpToolbar();
        navigationView = (NavigationView) findViewById(R.id.navigation_menu);

        //changing navigation menu depending on user type
        String userType = session.getUserType();
        //Log.e(TAG, "UserType: " + userType);
        if (userType.equals("Student")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.navigation_user);
        }
        if (userType.equals("Admin")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.navigation_admin);
        }
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        Intent siteIntent = new Intent(getApplicationContext(), DeptSiteActivity.class);
                        startActivity(siteIntent);
                        break;
                    case R.id.nav_add_notice:
                        Intent add_notice_intent = new Intent(getApplicationContext(), PublishNoticeActivity.class);
                        startActivity(add_notice_intent);
                        break;
                    case R.id.nav_allnotices:
                        Intent notice_modify_intent = new Intent(getApplicationContext(), NoticeModifyActivity.class);
                        startActivity(notice_modify_intent);
                        break;
                    case R.id.nav_logout:
                        logoutUser();
                        break;
                    case R.id.nav_notify:
                        Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_settings:
                        Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(i);
                        break;
                    case R.id.nav_send_notification:
                        Intent sendIntent = new Intent(getApplicationContext(), SendNotificationActivity.class);
                        startActivity(sendIntent);
                        break;
                    case R.id.nav_aboutus:
                        Intent intentAbout = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(intentAbout);
                        break;
                }

                return false;
            }
        });
    }
    //action menu setting

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(i);
        }

        if (id == R.id.about_us) {

            Intent notice = new Intent(getApplicationContext(), AboutActivity.class);
            startActivity(notice);
        }
        return super.onOptionsItemSelected(item);
    }

    private void setUpToolbar() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    private void logoutUser() {
        session.setLogin(false);

        // Launching the login activity
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    //intent transition
    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}
