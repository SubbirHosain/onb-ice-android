package com.subbirhosain.onbice.activity;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.util.Patterns;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.github.ybq.android.spinkit.SpinKitView;
import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.helper.SessionManager;
import com.subbirhosain.onbice.model.Categories;
import com.subbirhosain.onbice.model.Category;
import com.subbirhosain.onbice.model.FcmTopicResponse;
import com.subbirhosain.onbice.model.SendTopicResponse;
import com.subbirhosain.onbice.model.Topic;
import com.subbirhosain.onbice.network.ApiClient;
import com.subbirhosain.onbice.network.ApiService;

import java.util.ArrayList;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SendNotificationActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = SendNotificationActivity.class.getSimpleName();

    DrawerLayout drawerLayout;
    Toolbar toolbar;
    ActionBarDrawerToggle actionBarDrawerToggle;
    NavigationView navigationView;
    private SessionManager session;

    private EditText noticeTitle, noticeMessage, noticeLink;
    private Spinner noticeBatch;

    private Button submitNotification;
    List<String> listSpinner;
    private AwesomeValidation awesomeValidation;
    SpinKitView progressNotification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_notification);

        //validate url
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);
        awesomeValidation.addValidation(this, R.id.etNotificationLink, Patterns.WEB_URL, R.string.invalid_url);

        noticeTitle = findViewById(R.id.etNotificationTitle);
        noticeMessage = findViewById(R.id.etNotificationMessage);
        noticeLink = findViewById(R.id.etNotificationLink);
        noticeBatch = findViewById(R.id.spinnerTopics);
        submitNotification = findViewById(R.id.btnSendPush);

        submitNotification.setOnClickListener(this);

        progressNotification = findViewById(R.id.spin_kit);

        //load fcm topics
        loadFcmTopics();
        noticeBatch.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });
        session = new SessionManager(getApplicationContext());
        if (!session.isLoggedIn()) {
            logoutUser();
        }

        setUpToolbar();
        navigationView = (NavigationView) findViewById(R.id.navigation_menu);

        //changing navigation menu depending on user type
        String userType = session.getUserType();
        //Log.e(TAG, "UserType: " + userType);
        if (userType.equals("Student")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.navigation_user);
        }
        if (userType.equals("Admin")) {
            navigationView.getMenu().clear();
            navigationView.inflateMenu(R.menu.navigation_admin);
        }
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_home:
                        Intent siteIntent = new Intent(getApplicationContext(), DeptSiteActivity.class);
                        startActivity(siteIntent);
                        break;
                    case R.id.nav_add_notice:
                        Intent add_notice_intent = new Intent(getApplicationContext(), PublishNoticeActivity.class);
                        startActivity(add_notice_intent);
                        break;
                    case R.id.nav_allnotices:
                        Intent notice_modify_intent = new Intent(getApplicationContext(), NoticeModifyActivity.class);
                        startActivity(notice_modify_intent);
                        break;
                    case R.id.nav_logout:
                        logoutUser();
                        break;
                    case R.id.nav_notify:
                        Intent intent = new Intent(getApplicationContext(), NotificationActivity.class);
                        startActivity(intent);
                        break;
                    case R.id.nav_settings:
                        Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
                        startActivity(i);
                        break;
                    case R.id.nav_send_notification:
                        Intent sendIntent = new Intent(getApplicationContext(), SendNotificationActivity.class);
                        startActivity(sendIntent);
                        break;
                    case R.id.nav_aboutus:
                        Intent intentAbout = new Intent(getApplicationContext(), AboutActivity.class);
                        startActivity(intentAbout);
                        break;
                }

                return false;
            }
        });
    }

    //action menu setting

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.action_menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            Intent i = new Intent(getApplicationContext(), SettingsActivity.class);
            startActivity(i);
        }

        if (id == R.id.about_us) {

            Intent notice = new Intent(getApplicationContext(), AboutActivity.class);
            startActivity(notice);
        }
        return super.onOptionsItemSelected(item);
    }


    private void setUpToolbar() {
        drawerLayout = (DrawerLayout) findViewById(R.id.drawerLayout);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    private void logoutUser() {
        session.setLogin(false);

        // Launching the login activity
        Intent intent = new Intent(getApplicationContext(), LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void onClick(View view) {

        if (view == submitNotification) {
            sendBatchNotification();
        }
    }

    private void sendBatchNotification() {
        String title = noticeTitle.getText().toString().trim();
        String message = noticeMessage.getText().toString().trim();
        String link = noticeLink.getText().toString().trim();
        String topic = noticeBatch.getSelectedItem().toString();

        if (link.isEmpty()) {
            link = "";
        }
        if (!title.isEmpty() && !message.isEmpty()) {

            ApiService service = ApiClient.getClient().create(ApiService.class);
            Call<ResponseBody> call = service.sendTopicNotification(title, message, topic, link);
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    Log.e(TAG, "Login Error: " + response.body());
                    if (response.isSuccessful()) {
                        Toast.makeText(SendNotificationActivity.this, "Sent", Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {

                }
            });
        } else {
            Toast.makeText(getApplicationContext(), "Please Title and Message!", Toast.LENGTH_LONG).show();
        }
    }

    //load fcm spinner data from remote db
    private void loadFcmTopics() {

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<FcmTopicResponse> call = service.getFcmTopics();

        call.enqueue(new Callback<FcmTopicResponse>() {
            @Override
            public void onResponse(Call<FcmTopicResponse> call, Response<FcmTopicResponse> response) {
                List<Topic> topicList = response.body().getTopics();
                listSpinner = new ArrayList<String>();

                for (int i = 0; i < topicList.size(); i++) {
                    listSpinner.add(topicList.get(i).getTopicName());
                }
                //set the adapter
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(SendNotificationActivity.this,
                        android.R.layout.simple_spinner_item, listSpinner);

                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                noticeBatch.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<FcmTopicResponse> call, Throwable t) {

            }
        });

    }

    //intent transition
    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}
