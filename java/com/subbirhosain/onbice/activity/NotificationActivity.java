package com.subbirhosain.onbice.activity;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.helper.SharedPrefManager;
import com.subbirhosain.onbice.model.RegisterDevices;
import com.subbirhosain.onbice.network.ApiClient;
import com.subbirhosain.onbice.network.ApiService;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NotificationActivity extends AppCompatActivity implements View.OnClickListener {
    private TextView textView;
    Button btn;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notification);

        textView = findViewById(R.id.textView);
        btn = findViewById(R.id.button2);
        btn.setOnClickListener(this);
    }


    @Override
    public void onClick(View view) {
        //getting fcm token and user email from sharedPref
        String token = SharedPrefManager.getInstance(this).getDeviceToken();
        String email = SharedPrefManager.getInstance(getApplicationContext()).getUserEmail();

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<RegisterDevices> call = service.sendRegToServer(email,token);

        call.enqueue(new Callback<RegisterDevices>() {
            @Override
            public void onResponse(@NonNull Call<RegisterDevices> call, @NonNull Response<RegisterDevices> response) {

                    String str = response.body().getFcmMessage();
                    Toast.makeText(NotificationActivity.this, str, Toast.LENGTH_LONG).show();

            }

            @Override
            public void onFailure(@NonNull Call<RegisterDevices> call, @NonNull Throwable t) {
                //Toast.makeText(NotificationActivity.this, t.getMessage().toString(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
