package com.subbirhosain.onbice.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;

import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.model.DataReceiveEvent;
import com.subbirhosain.onbice.model.Notice;
import com.subbirhosain.onbice.model.SearchNoticeResponse;
import com.subbirhosain.onbice.network.ApiClient;
import com.subbirhosain.onbice.network.ApiService;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NoticeSearchActivity extends AppCompatActivity implements View.OnClickListener {
    //private final Context mcontext=this;
    private static final String TAG = NoticeSearchActivity.class.getSimpleName();
    private Button btnSearch;
    private EditText fromDate, toDate;
    private DatePickerDialog noticeDatePicker;
    private SimpleDateFormat dateFormatter;

    private ProgressDialog pDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_notice_search);

        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        btnSearch = findViewById(R.id.searchNotice);
        fromDate = findViewById(R.id.dateFrom);
        toDate = findViewById(R.id.dateTo);

        fromDate.setOnClickListener(this);
        toDate.setOnClickListener(this);
        btnSearch.setOnClickListener(this);

        //date dealing
        dateFormatter = new SimpleDateFormat("yyy-MM-dd", Locale.US);

    }

    @Override
    public void onClick(View view) {
        if (view == fromDate) {
            chooseDate(fromDate);
        }
        if (view == toDate) {
            chooseDate(toDate);
        }
        if (view == btnSearch) {
            searchNotice();

        }
    }

    private void searchNotice() {

        String from_date = fromDate.getText().toString();
        String to_date = toDate.getText().toString();

        pDialog.setMessage("Searching ...");
        //showDialog();

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<SearchNoticeResponse> call = service.getOldNotices(from_date, to_date);

        call.enqueue(new Callback<SearchNoticeResponse>() {
            @Override
            public void onResponse(Call<SearchNoticeResponse> call, Response<SearchNoticeResponse> response) {

                List<Notice> noticeList = response.body().getNotices();

                EventBus.getDefault().post(new DataReceiveEvent("data_received", noticeList, "ok"));
            }

            @Override
            public void onFailure(Call<SearchNoticeResponse> call, Throwable t) {

            }
        });
        Intent another = new Intent(NoticeSearchActivity.this, ShowSearchedNoticeActivity.class);
        startActivity(another);
    }

    private void chooseDate(final EditText field) {
        //dealing with current time
        Calendar newCalendar = Calendar.getInstance();
        noticeDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(i, i1, i2);
                field.setText(dateFormatter.format(newDate.getTime()));

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));
        noticeDatePicker.show();
    }

    //show and hide dialog
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }
}
