package com.subbirhosain.onbice.activity;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.content.FileProvider;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.karumi.dexter.listener.single.PermissionListener;
import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.helper.RealPathUtil;
import com.subbirhosain.onbice.model.AddNotice;
import com.subbirhosain.onbice.model.Categories;
import com.subbirhosain.onbice.model.Category;
import com.subbirhosain.onbice.model.FileUpload;
import com.subbirhosain.onbice.network.ApiClient;
import com.subbirhosain.onbice.network.ApiService;

import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddNoitceActivity extends AppCompatActivity implements View.OnClickListener {

    private static final String TAG = AddNoitceActivity.class.getSimpleName();
    private ProgressDialog pDialog;

    List<String> listSpinner;
    List<String> spinnerId;
    String sessionId;

    private EditText etTextArea;
    private EditText etNoticeTitle;
    private Spinner noticeSpinner;

    private EditText eTdatePicker;

    private DatePickerDialog noticeDatePicker;
    private SimpleDateFormat dateFormatter;

    private Button btnSubmit, btnPickImage;
    ImageView noticeImage;
    private int REQUEST_CAMERA = 0, SELECT_FILE = 1;
    private String userChoosenTask;
    private String selectedFilePath = ""; //to store the real path of the selected image
    private String imageReturnedUrl = ""; //hold the featured image after the upload
    private ProgressBar progressImage;
    private String imageFilePath = "";
    //variables for kitkat image capture
    private static final String IMAGE_DIRECTORY_NAME = "VLEMONN";
    static final int CAPTURE_IMAGE_REQUEST = 5;
    File photoFile = null;
    private File compressedImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_noitce);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        //date dealing
        dateFormatter = new SimpleDateFormat("yyy-MM-dd", Locale.US);

        etTextArea = (EditText) findViewById(R.id.textArea_information);
        noticeSpinner = (Spinner) findViewById(R.id.add_notice_spinner);
        eTdatePicker = (EditText) findViewById(R.id.etDate);
        btnSubmit = (Button) findViewById(R.id.noticePublish);
        etNoticeTitle = (EditText) findViewById(R.id.etTitle);
        btnPickImage = (Button) findViewById(R.id.pickFeaturedImage);
        noticeImage = (ImageView)findViewById(R.id.noticeImage);
        progressImage = (ProgressBar)findViewById(R.id.imageUploadProgress);

        eTdatePicker.setOnClickListener(this);
        btnSubmit.setOnClickListener(this);
        btnPickImage.setOnClickListener(this);

        etTextArea.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View view, MotionEvent motionEvent) {
                view.getParent().requestDisallowInterceptTouchEvent(true);
                switch (motionEvent.getAction() & MotionEvent.ACTION_MASK) {
                    case MotionEvent.ACTION_UP:
                        view.getParent().requestDisallowInterceptTouchEvent(false);
                        break;
                }
                return false;
            }
        });

        //load spinner data from remote db
        loadSpinnerData();

        noticeSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String selectedName = adapterView.getItemAtPosition(i).toString();
                sessionId = spinnerId.get(i);

                //Toast.makeText(getApplicationContext(), "value:" + sessionId, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        //dealing with current time
        Calendar newCalendar = Calendar.getInstance();
        noticeDatePicker = new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int i, int i1, int i2) {
                Calendar newDate = Calendar.getInstance();
                newDate.set(i, i1, i2);
                eTdatePicker.setText(dateFormatter.format(newDate.getTime()));

            }
        }, newCalendar.get(Calendar.YEAR), newCalendar.get(Calendar.MONTH), newCalendar.get(Calendar.DAY_OF_MONTH));

        //Create options Dialog box

    }
    //onCreate ends here
    //load spinner data from remote db
    private void loadSpinnerData() {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    ApiService service = ApiClient.getClient().create(ApiService.class);
                    Call<Categories> call = service.getBatches();

                    call.enqueue(new Callback<Categories>() {
                        @Override
                        public void onResponse(Call<Categories> call, Response<Categories> response) {
                            List<Category> catItem = response.body().getCategory();
                            //spinner data value
                            //List<String> listSpinner = new ArrayList<String>();
                            listSpinner = new ArrayList<String>();

                            //spinnner data db id
                            //List<String> spinnerId = new ArrayList<String>();
                            spinnerId = new ArrayList<String>();

                            for (int i = 0; i < catItem.size(); i++) {
                                listSpinner.add(catItem.get(i).getNoticeCatSession());
                                spinnerId.add(catItem.get(i).getNoticeCatId());
                            }

                            //set the adapter
                            ArrayAdapter<String> adapter = new ArrayAdapter<String>(AddNoitceActivity.this,
                                    android.R.layout.simple_spinner_item, listSpinner);

                            adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            noticeSpinner.setAdapter(adapter);

                        }

                        @Override
                        public void onFailure(Call<Categories> call, Throwable t) {

                        }
                    });
                }
            }).start();
    }

    @Override
    public void onClick(View view) {
        if (view == eTdatePicker) {
            noticeDatePicker.show();
            ;
        }
        if (view == btnSubmit) {
            addNotice();
        }
        if (view == btnPickImage) {
            selectImage();
        }
    }

    //getting notice contents
    private void addNotice() {

        String notice_title = etNoticeTitle.getText().toString().trim();
        String notice_content = etTextArea.getText().toString();
        String notice_date = eTdatePicker.getText().toString();
        String notice_featured_image = imageReturnedUrl;
        String notice_category = sessionId;

        if (!notice_title.isEmpty() && !notice_content.isEmpty()) {

            pDialog.setMessage("Adding Notice ...");
            showDialog();

            ApiService service = ApiClient.getClient().create(ApiService.class);
            Call<AddNotice> call = service.addNotice(notice_title,notice_content,notice_category,notice_date,notice_featured_image);

            call.enqueue(new Callback<AddNotice>() {
                @Override
                public void onResponse(Call<AddNotice> call, Response<AddNotice> response) {
                    if (!response.body().getNoticeError()){
                        hideDialog();
                        Toast.makeText(getApplicationContext(), response.body().getNoticeMessage(), Toast.LENGTH_LONG).show();
                    }
                    else{
                        hideDialog();
                        Toast.makeText(getApplicationContext(), response.body().getNoticeMessage(), Toast.LENGTH_LONG).show();
                    }
                }

                @Override
                public void onFailure(Call<AddNotice> call, Throwable t) {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Failed To Publish", Toast.LENGTH_LONG).show();
                }
            });
        }
        else{
            Toast.makeText(getApplicationContext(),
                    "Please enter the notice!", Toast.LENGTH_LONG)
                    .show();
        }


    }

    //opening camera and processing photo
    private void selectImage() {
        //Setting options Dialog box
        final CharSequence[] items = {"Take Photo", "Choose from Library", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(AddNoitceActivity.this);
        builder.setTitle("Add Photo!");

        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                if (items[i].equals("Take Photo")) {
                    userChoosenTask = "Take Photo";
                    //open camear with permission
                    Dexter.withActivity(AddNoitceActivity.this)
                            .withPermissions(
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE,
                                    Manifest.permission.CAMERA)
                            .withListener(new MultiplePermissionsListener() {
                                @Override
                                public void onPermissionsChecked(MultiplePermissionsReport report) {
                                    // check if all permissions are granted
                                    if (report.areAllPermissionsGranted()) {

                                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                                            openCamera();
                                        }
                                        else{
                                            openCamera4();
                                        }
                                    }
                                    // check for permanent denial of any permission
                                    if (report.isAnyPermissionPermanentlyDenied()) {
                                        // show alert dialog navigating to Settings
                                        showSettingsDialog();
                                    }
                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                    token.continuePermissionRequest();
                                }
                            }).
                            withErrorListener(new PermissionRequestErrorListener() {
                                @Override
                                public void onError(DexterError error) {
                                    Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .onSameThread()
                            .check();
                } else if (items[i].equals("Choose from Library")) {
                    userChoosenTask = "Choose from Library";
                    //open gallery with permission
                    Dexter.withActivity(AddNoitceActivity.this)
                            .withPermissions(
                                    Manifest.permission.READ_EXTERNAL_STORAGE,
                                    Manifest.permission.WRITE_EXTERNAL_STORAGE)
                            .withListener(new MultiplePermissionsListener() {
                                @Override
                                public void onPermissionsChecked(MultiplePermissionsReport report) {
                                    // check if all permissions are granted
                                    if (report.areAllPermissionsGranted()) {
                                        openGallery();
                                    }
                                    // check for permanent denial of any permission
                                    if (report.isAnyPermissionPermanentlyDenied()) {
                                        // show alert dialog navigating to Settings
                                        showSettingsDialog();
                                    }
                                }

                                @Override
                                public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                                    token.continuePermissionRequest();
                                }
                            }).
                            withErrorListener(new PermissionRequestErrorListener() {
                                @Override
                                public void onError(DexterError error) {
                                    Toast.makeText(getApplicationContext(), "Error occurred! ", Toast.LENGTH_SHORT).show();
                                }
                            })
                            .onSameThread()
                            .check();

                } else if (items[i].equals("Cancel")) {
                    dialogInterface.dismiss();
                }
            }
        });
        builder.show();
    }

    //openCamera for lolipop and above
    private void openCamera() {
        Intent pictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        pictureIntent.putExtra( MediaStore.EXTRA_FINISH_ON_COMPLETION, true);

        if (pictureIntent.resolveActivity(getPackageManager()) != null) {

            //Create a file to store the image
            File photoFile = null;
            try {
                photoFile = createImageFile();
            }
            catch (IOException e) {
                // Error occurred while creating the File
                Toast.makeText(this, "Photo file can't be created, please try again",
                        Toast.LENGTH_SHORT).show();
                return;
            }

            if (photoFile != null){
                Uri photoUri = FileProvider.getUriForFile(this, getPackageName() +".provider", photoFile);
                pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoUri);
                startActivityForResult(pictureIntent, REQUEST_CAMERA);
            }
        }
    }

    //open camera for kiatkat and lower
    private void openCamera4(){
        try {
            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            photoFile = createImageFile4();
            if(photoFile!=null)
            {
                //displayMessage(getBaseContext(),photoFile.getAbsolutePath());
                Log.i("Mayank",photoFile.getAbsolutePath());
                Uri photoURI  = Uri.fromFile(photoFile);
                Log.i("Mayank",photoURI.toString());

                selectedFilePath = photoFile.getAbsolutePath();
                cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                startActivityForResult(cameraIntent, CAPTURE_IMAGE_REQUEST);
            }
        }
        catch (Exception e)
        {
            Toast.makeText(this,"Camera is not available",Toast.LENGTH_LONG).show();
        }
    }

    private void openGallery(){
        Intent photoPickerIntent  = new Intent();
        photoPickerIntent .setType("image/*");
        photoPickerIntent .setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(photoPickerIntent, "Choose File to Upload.."),SELECT_FILE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK){
            if (requestCode == SELECT_FILE){
                //Handling Gallery Request For Selecting Image
                onSelectFromGalleryResult(data);
            }
            else if(requestCode == REQUEST_CAMERA){

                File imgFile = new  File(selectedFilePath);
                if(imgFile.exists())            {
                    //noticeImage.setImageURI(Uri.fromFile(imgFile));
                    //noticeImage.setVisibility(View.VISIBLE);
                    //start uploading here
                    uploadImage();
                }
            }
            else if(requestCode == CAPTURE_IMAGE_REQUEST){
                File imgFile = new  File(selectedFilePath);
                if(imgFile.exists()){
                    Glide.with(this)
                            .load(selectedFilePath)
                            .into(noticeImage);
                    noticeImage.setVisibility(View.VISIBLE);
                    uploadImage();
                }
            }
        }

    }
    private void onSelectFromGalleryResult(Intent data) {
        if (data == null){
            //no data present
            return;
        }
        //Relative content uri path
        Uri selectedFileUri = data.getData();

        //getting real path from uri
        selectedFilePath = RealPathUtil.getRealPath(this,selectedFileUri);

        Log.e(TAG,"Selected File Path:" + selectedFilePath);
        Log.e(TAG,"Selected File Path:" + selectedFileUri);
        //Logger.d("SelectedPath: "+ selectedFilePath);
        //setting the image to the hidden view
        if (selectedFilePath != null && !selectedFilePath.equals("")){
            //noticeImage.setImageURI(selectedFileUri);
            //noticeImage.setVisibility(View.VISIBLE);
            //setImage and make visibility visible here
            //start uploading here if need compress using any library
            //uploadImage();
            Log.e(TAG,"Selected File Path:" + selectedFilePath);

            Glide.with(this)
                    .load(selectedFileUri)
                    .into(noticeImage);
            noticeImage.setVisibility(View.VISIBLE);
            uploadImage();
        }
        else{
            Toast.makeText(this,"Cannot upload file to server",Toast.LENGTH_SHORT).show();
        }
    }
    //creating file for lolipop and above
    private File createImageFile() throws IOException{

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss", Locale.getDefault()).format(new Date());
        String imageFileName = "IMG_" + timeStamp + "_";
        File storageDir = getExternalFilesDir(Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(imageFileName, ".jpg", storageDir);
        selectedFilePath = image.getAbsolutePath();
        return image;
    }

    //creating file for kitkat and lower
    private File createImageFile4()
    {
        // External sdcard location
        File mediaStorageDir = new File(Environment
                        .getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES),
                IMAGE_DIRECTORY_NAME);
        // Create the storage directory if it does not exist
        if (!mediaStorageDir.exists()) {
            if (!mediaStorageDir.mkdirs()) {
                Toast.makeText(this,"Unable to create directory",Toast.LENGTH_LONG).show();
                return null;
            }
        }

        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss",Locale.getDefault()).format(new Date());
        File mediaFile = new File(mediaStorageDir.getPath() + File.separator + "IMG_" + timeStamp + ".jpg");
        return mediaFile;

    }



    private void onCaptureImageResult(Intent data) {


        /*    if (data != null && data.getExtras() != null) {
            Bitmap imageBitmap = (Bitmap) data.getExtras().get("data");
            noticeImage.setVisibility(View.VISIBLE);
            noticeImage.setImageBitmap(imageBitmap);
        }
        */
        //Glide.with(this).load(imageFilePath).into(noticeImage);
        // If you are using Glide.

        //noticeImage.setImageURI(Uri.parse(imageFilePath));
        //Toast.makeText(this, imageFilePath, Toast.LENGTH_SHORT).show();
        //start uploading here

    }
    //openGallery

    private void uploadImage() {

        File file = new File(selectedFilePath);
        RequestBody requestBody = RequestBody.create(MediaType.parse("image/*"),file);
        MultipartBody.Part fileToUpload = MultipartBody.Part.createFormData("file",file.getName(),requestBody);
        RequestBody filename = RequestBody.create(MediaType.parse("text/plain"), file.getName());

        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<FileUpload> call = service.uploadImage(fileToUpload,filename);

        progressImage.setVisibility(View.VISIBLE);

        call.enqueue(new Callback<FileUpload>() {
            @Override
            public void onResponse(Call<FileUpload> call, Response<FileUpload> response) {
                if (response.body().getSuccess()){
                    progressImage.setVisibility(View.GONE);
                    imageReturnedUrl = response.body().getImageUrl();
                    Log.e(TAG,"Returned URL:" + imageReturnedUrl);
                }
            }

            @Override
            public void onFailure(Call<FileUpload> call, Throwable t) {
            }
        });
    }

    /**
     * Showing Alert Dialog with Settings option
     * Navigates user to app settings
     * NOTE: Keep proper title and message depending on your app
     */
    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(AddNoitceActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();
    }

    // navigating user to app settings
    private void openSettings() {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

    private void displayMessage(Context context, String message)
    {
        Toast.makeText(context,message,Toast.LENGTH_LONG).show();
    }

    //show and hide dialog
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

}