package com.subbirhosain.onbice.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.adapter.NoticeSearchAdapter;
import com.subbirhosain.onbice.model.DataReceiveEvent;
import com.subbirhosain.onbice.model.Notice;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.List;

public class ShowSearchedNoticeActivity extends AppCompatActivity {

    private static final String TAG = ShowSearchedNoticeActivity.class.getSimpleName();
    private List<Notice> noticeList;

    RecyclerView recyclerView;
    LinearLayoutManager layoutManager;
    NoticeSearchAdapter searchAdapter;

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onDataReceiveEvent(DataReceiveEvent event) {

        noticeList = event.getNoticeList();
        searchAdapter = new NoticeSearchAdapter(ShowSearchedNoticeActivity.this, noticeList);
        recyclerView.setAdapter(searchAdapter);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_searched_notice);

        EventBus.getDefault().register(this);

        recyclerView = (RecyclerView) findViewById(R.id.postList);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

    }


    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


}