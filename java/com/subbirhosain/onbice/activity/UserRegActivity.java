package com.subbirhosain.onbice.activity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import com.basgeekball.awesomevalidation.AwesomeValidation;
import com.basgeekball.awesomevalidation.ValidationStyle;
import com.subbirhosain.onbice.R;
import com.subbirhosain.onbice.model.Categories;
import com.subbirhosain.onbice.model.Category;
import com.subbirhosain.onbice.model.RegisterUser;
import com.subbirhosain.onbice.network.ApiClient;
import com.subbirhosain.onbice.network.ApiService;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserRegActivity extends AppCompatActivity implements View.OnClickListener {
    Toolbar toolbar;
    List<String> listSpinner;
    List<String> spinnerId;
    String sessionId;

    private ProgressDialog pDialog;

    private EditText fname;
    private EditText lname;

    private EditText email;

    private EditText password;
    private EditText confirmPassword;

    private Spinner spinner;

    private Button submit;

    //defining AwesomeValidation object
    private AwesomeValidation awesomeValidation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_reg);
        //initializing awesomevalidation object
        awesomeValidation = new AwesomeValidation(ValidationStyle.BASIC);

        // Progress dialog
        pDialog = new ProgressDialog(this);
        pDialog.setCancelable(false);

        //initializing view objects
        fname = findViewById(R.id.etFname);
        lname = findViewById(R.id.etLname);
        email = findViewById(R.id.etEmail);
        password = findViewById(R.id.etPassword);
        confirmPassword = findViewById(R.id.etConfirmPassword);
        spinner = (Spinner) findViewById(R.id.spinner);
        submit = findViewById(R.id.submit);

        //adding validation to edittexts
        awesomeValidation.addValidation(this, R.id.etFname, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.invalid_fname);
        awesomeValidation.addValidation(this, R.id.etLname, "^[A-Za-z\\s]{1,}[\\.]{0,1}[A-Za-z\\s]{0,}$", R.string.invalid_lname);

        awesomeValidation.addValidation(this, R.id.etEmail, Patterns.EMAIL_ADDRESS, R.string.invalid_email);

        String regexPassword = ".{5,}";
        awesomeValidation.addValidation(this, R.id.etPassword, regexPassword, R.string.invalid_password);
        awesomeValidation.addValidation(this, R.id.etConfirmPassword, R.id.etPassword, R.string.invalid_confirm_password);

        submit.setOnClickListener(this);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        toolbar.setTitle(R.string.app_name);

        //load spinner data from remote db
        loadSpinnerData();

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                String selectedName = adapterView.getItemAtPosition(i).toString();
                sessionId = spinnerId.get(i);

                //Toast.makeText(getApplicationContext(), "value:" + sessionId, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    //load spinner data from remote db
    private void loadSpinnerData() {
        ApiService service = ApiClient.getClient().create(ApiService.class);
        Call<Categories> call = service.getBatches();

        call.enqueue(new Callback<Categories>() {
            @Override
            public void onResponse(Call<Categories> call, Response<Categories> response) {
                List<Category> catItem = response.body().getCategory();
                //spinner data value
                //List<String> listSpinner = new ArrayList<String>();
                listSpinner = new ArrayList<String>();

                //spinnner data db id
                //List<String> spinnerId = new ArrayList<String>();
                spinnerId = new ArrayList<String>();

                for (int i = 0; i < catItem.size(); i++) {
                    listSpinner.add(catItem.get(i).getNoticeCatSession());
                    spinnerId.add(catItem.get(i).getNoticeCatId());
                }

                //set the adapter
                ArrayAdapter<String> adapter = new ArrayAdapter<String>(UserRegActivity.this,
                        android.R.layout.simple_spinner_item, listSpinner);
                spinner.setPrompt("Select Session");
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spinner.setAdapter(adapter);

            }

            @Override
            public void onFailure(Call<Categories> call, Throwable t) {
                Toast.makeText(UserRegActivity.this, "Failed to load", Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void submitForm() {
        //first validate the form then move ahead
        //if this becomes true that means validation is successfull
        if (awesomeValidation.validate()) {

            //process the data further
            String u_fname = fname.getText().toString().trim();
            String u_lname = lname.getText().toString().trim();

            String user_email = email.getText().toString().trim();

            String user_password = password.getText().toString().trim();

            String user_session = sessionId;

            //Log.e("UserRegActivity",user_session);

            pDialog.setMessage("Registering ...");
            showDialog();

            ApiService service = ApiClient.getClient().create(ApiService.class);
            Call<RegisterUser> call = service.registerUser(user_email, u_fname, u_lname, user_password, user_session);
            call.enqueue(new Callback<RegisterUser>() {
                @Override
                public void onResponse(Call<RegisterUser> call, Response<RegisterUser> response) {

                    if (!response.body().getRegisterError()) {
                        //Log.e("UserRegActivity", response.body().toString());

                        hideDialog();
                        Toast.makeText(getApplicationContext(), response.body().getRegisterMessage(), Toast.LENGTH_LONG).show();

                        new Handler().postDelayed(new Runnable() {
                            @Override
                            public void run() {
                                //Redirect after 100ms
                                Intent intentLogin = new Intent(getApplicationContext(), LoginActivity.class);
                                startActivity(intentLogin);
                                finish();
                            }
                        }, 1500);

                    } else {
                        hideDialog();
                        Toast.makeText(getApplicationContext(), response.body().getRegisterMessage(), Toast.LENGTH_LONG).show();
                    }

                }

                @Override
                public void onFailure(Call<RegisterUser> call, Throwable t) {
                    pDialog.dismiss();
                    Toast.makeText(getApplicationContext(), "Failed To Register", Toast.LENGTH_LONG).show();
                }
            });
            //clearing the form fields
            awesomeValidation.clear();
        } else {
            Toast.makeText(getApplicationContext(), "Please Correct the form to be registered", Toast.LENGTH_LONG).show();
        }

    }

    @Override
    public void onClick(View view) {
        if (view == submit) {
            submitForm();
        }
    }

    //show and hide dialog
    private void showDialog() {
        if (!pDialog.isShowing())
            pDialog.show();
    }

    private void hideDialog() {
        if (pDialog.isShowing())
            pDialog.dismiss();
    }

    //intent transition
    @Override
    public void finish() {
        super.finish();
        overridePendingTransitionExit();
    }

    @Override
    public void startActivity(Intent intent) {
        super.startActivity(intent);
        overridePendingTransitionEnter();
    }

    /**
     * Overrides the pending Activity transition by performing the "Enter" animation.
     */
    protected void overridePendingTransitionEnter() {
        overridePendingTransition(R.anim.slide_from_right, R.anim.slide_to_left);
    }

    /**
     * Overrides the pending Activity transition by performing the "Exit" animation.
     */
    protected void overridePendingTransitionExit() {
        overridePendingTransition(R.anim.slide_from_left, R.anim.slide_to_right);
    }
}
